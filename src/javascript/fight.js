export function fight(firstFighter, secondFighter) {
  let queue = true;
  let firstFighterHealth = firstFighter.health;
  let secondFighterHealth = secondFighter.health;

  while(firstFighterHealth > 0  && secondFighterHealth > 0) {
    if(queue) {
      secondFighterHealth -= getDamage(firstFighter, secondFighter);
    } else {
      firstFighterHealth -= getDamage(secondFighter, firstFighter);
    }
    queue = !queue;
  }

  return firstFighterHealth > secondFighterHealth ? firstFighter : secondFighter;
}

export function getDamage(attacker, enemy) {
  const damage = getHitPower(attacker) - getBlockPower(enemy);
  return damage <= 0 ? 0 : damage; 
}

export function getHitPower(fighter) {
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  return fighter.defense * (Math.random() + 1);
}
