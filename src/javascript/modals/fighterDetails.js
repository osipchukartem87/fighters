import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, image, source } = fighter;
  const attributes = { src: source };

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const list = createElement({ tagName: 'ul', className: 'list' });
  const nameElement = createElement({ tagName: 'li', className: 'fighter-name' });
  const attackElement = createElement({ tagName: 'li', className: 'fighter-attack' });
  const defenseElement = createElement({ tagName: 'li', className: 'fighter-defense' });
  const healthElement = createElement({ tagName: 'li', className: 'fighter-health' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });

  nameElement.innerText = name;
  attackElement.innerText = 'attack - ' + attack;
  defenseElement.innerText = 'defense - ' + defense;
  healthElement.innerText = 'health - ' + health;
  imageElement.innerHtml = 'image - ' + image;

  fighterDetails.append(list);
  list.append(nameElement);
  list.append(attackElement);
  list.append(defenseElement);
  list.append(healthElement);
  fighterDetails.append(imageElement);

  return fighterDetails;
}
