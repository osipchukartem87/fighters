import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showWinnerModal(fighter) {
  const title = 'Winner: ' + fighter.name;
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });
}

function createWinnerDetails(fighter) {
  const { name, image, source } = fighter;
  const attributes = { src: source };

  const winnerModal = createElement({ tagName: 'div', className: 'modal-body' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });

  imageElement.innerHtml = 'image - ' + image;

  winnerModal.append(imageElement);
  
  return winnerModal;
}